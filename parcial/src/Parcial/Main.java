package Parcial;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Palabra p1 = new Palabra();
        p1.setNombre("Istituto");
        p1.setTipo("Sustantivo");
        ArrayList<Palabra> aux1 = new ArrayList<>();
        aux1.add(p1);

        Leccion l1 = new Leccion();
        l1.setPalabras(aux1);
        l1.setResumen("Istituto es un sustantivo que significa instituto");
        Leccion l2 = new Leccion();
        l2.setPalabras(aux1);
        l2.setResumen("Recordando Istituto es un sustantivo");
        ArrayList<Leccion> aux2 = new ArrayList<>();
        aux2.add(l1);
        aux2.add(l2);

        Curso c1 = new Curso("Italiano", "Basico", aux2);
        ArrayList<Curso> aux3 = new ArrayList<>();
        aux3.add(c1);

        Estudiante e1 = new Estudiante("Juan", aux3);
        Estudiante e2 = new Estudiante("Jorge", aux3);
        ArrayList<Estudiante> aux4 = new ArrayList<>();
        aux4.add(e1);
        aux4.add(e2);

        Programa programa = new Programa("ProgramaPrueba", 1, aux3, 2, aux4);

        for (int i = 0; i < 2; i++) {
            System.out.println("Para el estudiante " + programa.getEstudiantes().get(i).getNombre()
                    + " sus resumenes son: ");
            for(int j=0; j<2; j++){
                System.out.println(programa.getEstudiantes().get(i).getCursos()
                        .get(0).getLecciones().get(j).getResumen());
            }
            System.out.println("Y sus ejercicios son: ");
            for(int j=0; j<2; j++){
                System.out.println(programa.getEstudiantes().get(i).getCursos()
                        .get(0).getLecciones().get(j).getPalabras().get(0).getNombre()+" ¿que tipo de palabra es?: ");
            }
            System.out.println("\t");

        }
    }
}