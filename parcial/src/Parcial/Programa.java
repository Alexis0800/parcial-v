package Parcial;

import java.util.ArrayList;

public class Programa {
    private String Nombre;
    private Integer nCursos;
    private ArrayList<Curso> Cursos;
    private Integer nEstudiantes;
    private ArrayList<Estudiante> Estudiantes;

    public Programa(String nombre, Integer nCursos, ArrayList<Curso> cursos, Integer nEstudiantes, ArrayList<Estudiante> estudiantes) {
        Nombre = nombre;
        this.nCursos = nCursos;
        Cursos = cursos;
        this.nEstudiantes = nEstudiantes;
        Estudiantes = estudiantes;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public Integer getnCursos() {
        return nCursos;
    }

    public void setnCursos(Integer nCursos) {
        this.nCursos = nCursos;
    }

    public ArrayList<Curso> getCursos() {
        return Cursos;
    }

    public void setCursos(ArrayList<Curso> cursos) {
        Cursos = cursos;
    }

    public Integer getnEstudiantes() {
        return nEstudiantes;
    }

    public void setnEstudiantes(Integer nEstudiantes) {
        this.nEstudiantes = nEstudiantes;
    }

    public ArrayList<Estudiante> getEstudiantes() {
        return Estudiantes;
    }

    public void setEstudiantes(ArrayList<Estudiante> estudiantes) {
        Estudiantes = estudiantes;
    }
}
