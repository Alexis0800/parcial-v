package Parcial;

import java.util.ArrayList;

public class Leccion {
    private ArrayList<Palabra> Palabras;
    private String Resumen;

    public ArrayList<Palabra> getPalabras() {
        return Palabras;
    }

    public void setPalabras(ArrayList<Palabra> palabras) {
        Palabras = palabras;
    }

    public String getResumen() {
        return Resumen;
    }

    public void setResumen(String resumen) {
        Resumen = resumen;
    }
}
