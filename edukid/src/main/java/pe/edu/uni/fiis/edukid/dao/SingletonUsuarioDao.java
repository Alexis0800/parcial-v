package pe.edu.uni.fiis.edukid.dao;

import pe.edu.uni.fiis.edukid.dao.usuario.Usuariodao;
import pe.edu.uni.fiis.edukid.dao.usuario.UsuariodaoImpl;

public abstract class SingletonUsuarioDao {
    private static Usuariodao usuariodao=null;
    public static Usuariodao getUsuariodao(){
        if (usuariodao==null){
            usuariodao=new UsuariodaoImpl();
        }
        return usuariodao;
    }
}
