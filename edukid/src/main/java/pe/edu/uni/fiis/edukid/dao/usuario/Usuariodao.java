package pe.edu.uni.fiis.edukid.dao.usuario;

import pe.edu.uni.fiis.edukid.model.Usuario;

import java.sql.Connection;

public interface Usuariodao {
    public Usuario agregarUsuario(Usuario a, Connection b);
}
