package Parcial;

import java.util.ArrayList;

public class Curso {
    private String Nombre;
    private String Nivel;
    private ArrayList<Leccion> Lecciones;

    public Curso(String nombre, String nivel, ArrayList<Leccion> lecciones) {
        Nombre = nombre;
        Nivel = nivel;
        Lecciones = lecciones;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNivel() {
        return Nivel;
    }

    public void setNivel(String nivel) {
        Nivel = nivel;
    }

    public ArrayList<Leccion> getLecciones() {
        return Lecciones;
    }

    public void setLecciones(ArrayList<Leccion> lecciones) {
        Lecciones = lecciones;
    }
}
