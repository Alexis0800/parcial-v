package Parcial;

import java.util.ArrayList;

public class Estudiante {
    private String Nombre;
    private ArrayList<Curso> Cursos;

    public Estudiante(String nombre, ArrayList<Curso> cursos) {
        Nombre = nombre;
        Cursos = cursos;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public ArrayList<Curso> getCursos() {
        return Cursos;
    }

    public void setCursos(ArrayList<Curso> cursos) {
        Cursos = cursos;
    }
}
